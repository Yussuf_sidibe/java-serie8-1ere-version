package org.SIDIBE.Serie8.exo16;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		Function<Person,byte[]> personToByteArray= p->{
			byte [] tab = null;
			System.out.println("1 :"+tab);
			try(ByteArrayOutputStream bos=new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(bos);){
				dos.writeInt(p.getAge());
				dos.writeUTF(p.getFirstName());
				dos.writeUTF(p.getLastName());
				
				tab = bos.toByteArray();
			}catch (IOException e) {

				// affichage du message d'erreur et de la pile d'appel
				System.out.println("Erreur " + e.getMessage());
				e.printStackTrace();
			}
			System.out.println("2 : "+tab[15]);
			return tab;
		};
		PersonReader lecture = new PersonReader();
		PersonWriter ecriture = new PersonWriter();
		String fileName = "files/person.txt";
		String fileNameBinary = "files/personBis.txt";
		Person p1 = new Person("Youss","SIDIBE",12);
		System.out.println("resultat :"+personToByteArray.apply(p1));
		List<Person> people = Arrays.asList(new Person("Sidibe", "youss", 23), new Person("Mane", "Sadio", 32));
		ecriture.write(people, fileName);
		//Teste de la m�thode writeBinaryFields()
		System.out.println("Ecriture dans le fichier binaire !!!");
		ecriture.writeBinaryFields(people, fileNameBinary);
		//Teste de lq m�thode readBinaryFields()
		System.out.println("Voici les personnes qui sont lues � partir du fichier binaire: ");
		System.out.println(lecture.readBinaryFields(fileNameBinary));
		
	}

}
