package org.SIDIBE.Serie8.exo16;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersonReader {
	
	public List<Person> readBinaryFields(String fileName) throws FileNotFoundException{
		List<Person> list = new ArrayList<>();
		Person p = new Person();
		int nombrePersonne=0;
		try(FileInputStream fis= new FileInputStream(fileName);
			DataInputStream dis=new DataInputStream(fis);){
			nombrePersonne=dis.readInt();
			dis.readUTF();
			for(int i=0; i<nombrePersonne;i++)
			 {
				p.setLastName(dis.readUTF());;
				dis.readChar();
				p.setFirstName(dis.readUTF());
				dis.readChar();
				p.setAge(dis.readInt());
				dis.readUTF();
				list.add(p);
				p=new Person();
			}
		}catch(IOException e){
			e.getStackTrace();
			System.err.println(" La liste n'est pas remplit");
		}
		return list;
		
	}

}
