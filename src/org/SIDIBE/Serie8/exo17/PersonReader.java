package org.SIDIBE.Serie8.exo17;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PersonReader implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<Person> readBinaryObject(String fileName){
		List<Person> list = new ArrayList<>();
		try(FileInputStream fis=new FileInputStream(fileName);
			ObjectInputStream ois=new ObjectInputStream(fis);){
			list=(List<Person>) ois.readObject();
		}catch(IOException | ClassNotFoundException e){
			e.printStackTrace();
		}
		return list;
		
	}

}
