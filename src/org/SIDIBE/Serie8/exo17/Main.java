package org.SIDIBE.Serie8.exo17;

import java.util.Arrays;
import java.util.List;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PersonReader lecture = new PersonReader();
		PersonWriter ecriture = new PersonWriter();
		//String fileName = "files/person.txt";
		String fileNameBinaryObject = "files/personTer.txt";
		
		List<Person> people = Arrays.asList(new Person("SIDIBE", "Aicha", 23), new Person("MANE", "Samba", 32));
		//ecriture.write(people, fileName);

		//Teste de la m�thode writeBinaryObject()
				System.out.println("Ecriture dans le fichier binaire avec un flux ObjectOutputStream!!!");
				ecriture.writeBinaryObject(people, fileNameBinaryObject);
				//Teste de la m�thode readBinaryObject();
				System.out.println("Voici les personnes qui sont lues � partir du fichier binaire:");
				System.out.println(lecture.readBinaryObject(fileNameBinaryObject));
				//serialisation
				//ecriture.writeBinaryObject(people, fileNameBinaryObjectV1);
				//System.out.println("Nouvelle version"+lecture.readBinaryObject(fileNameBinaryObjectV1));
	}

}
